let userNum1 = prompt("Enter first number", "");
let operation = prompt("Enter operation", "");
let userNum2 = prompt("Enter second number", "");

while (true) {
  if (userNum1 === null) {
    alert("You've escaped", `${userNum1}`);
    console.log("You've escaped", `${userNum1}`);
    break;
  } else if (isNaN(userNum1) || isNaN(userNum2)) {
    alert("Not a number! Please, enter again");
    userNum1 = +prompt("Not a number! Enter first number", `${userNum1}`);
    userNum2 = +prompt("Not a number! Enter second number", `${userNum2}`);
  } else {
    break;
  }
}

function calc() {
  switch (operation) {
    case "+":
      result = +userNum1 + +userNum2;
      break;
    case "-":
      result = userNum1 - userNum2;
      break;
    case "*":
      result = userNum1 * userNum2;
      break;
    case "/":
      result = (userNum1 / userNum2).toFixed(2);
      break;
    default:
      return "Something has gone wrong";
  }
  console.log(
    `Your result from operation ${userNum1} ${operation} ${userNum2} is equal to ${result}`
  );
}
calc();
